<?php 
namespace App\Helpers;

use App\ListTemplate;

use App\EmailList;

use App\Template;

use App\Helpers\MailClient;

class EmailProcessor {

	const STATUS_NEW = "New";

	public static function start(){
		$runningListTemplates = ListTemplate::where(['status' => self::STATUS_NEW])->where("scheduled_at", "<=", date("Y-m-d H:i:s"))->get();

		foreach ($runningListTemplates as $key => $listTemplate) {
			$mailClient = new MailClient($listTemplate);
			$mailClient->sendMail();
		}	
	}

}

?>