<?php  

namespace App\Helpers;

use Swift_SmtpTransport;
use Swift_MailTransport;
use Swift_Mailer;
use Swift_Message;

use Illuminate\Support\Facades\Mail as Mail;

use Illuminate\Mail\Mailer as Mailer;

use Illuminate\Contracts\View\Factory;

use App\Template;

use App\EmailList;

use App\CampaignLog;

use Config;

use Exception;

class MailClient extends Mailer {

    // App\ListTemplate Object
    protected $listTemplate;

    protected $swiftMailer;

    protected $swiftTransporter;

    protected $from;

    protected $message;

    protected $replayTo;

    const STATUS_PROCESSING = "Processing";

    const STATUS_FAILED = "Failed";
    
    const STATUS_COMPLETE = "Complete";

    const TEMPLATE_PATH = "mail.templates";

    public function __construct(\App\ListTemplate $listTemplate, $partner = "sendGrid",$useDefaultConnection = false){
        $this->listTemplate = $listTemplate;

        if(empty($this->listTemplate->template)){
            $this->updateListTemplateStatus(self::STATUS_FAILED, "Template Missing");
            die;
        }

        if(empty($this->listTemplate->email_list)) {
            $this->updateListTemplateStatus(self::STATUS_FAILED, "List Missing");
            die;
        }


        $this->from = array(Config::get("mail.from")['address'] => Config::get("mail.from")['name']);

        $this->replayTo = array(Config::get("mail.replayTo")['address'] => Config::get("mail.replayTo")['name']);

        if($useDefaultConnection){
            // copy swift object from Parent Mailer Class
            $this->swiftMailer = $this->swift;
        }else {

            $this->setMailer();
        }
    }

	public function sendMail($from = [], $replayTo = []){

        if(!empty($from)){
            $this->from = $from;
        }

        if(!empty($replayTo)){
            $this->replayTo = $replayTo;
        }

        // before sending emails to list update the status to processing

        $this->updateListTemplateStatus(self::STATUS_PROCESSING);

        //$this->setMessage();

        if(empty($this->listTemplate->email_list->customer_emails)){
            $this->updateListTemplateStatus(self::STATUS_FAILED, "No Emails Exists For The list");
        }

        $sent = 0;
        $failures = 0;
        foreach ($this->listTemplate->email_list->customer_emails->reverse() as $customer) {
            if($customer->unsubscribed === 1){
                continue;
            }

            if($customer->bounced === 1){
                continue;
            }

            if($customer->active === 0){
                continue;
            }
            $status = "Failed";
            $comments = "";
            $messageId = "";
            try {

                // Replace a href url with require params like UTM params, redirect URL, template Id, Customer Id
                $requireParams = array("utm_campaign" => $this->listTemplate->template->name, "utm_medium" => "email", "utm_source" => "Zingmails", "to" => array($customer->email => $customer->name ), "replayTo" => $this->replayTo);

                $messageId = $this->setMessage($requireParams);

                $this->start();

                if($this->swiftMailer->send($this->message)){
                    $status = "Sent";
                    $sent++;
                }else {
                    $status = "Failed";
                    $failures++;
                    $this->setMailer();
                }
                $this->stop();
            } catch (Exception $e) {
                $status = "Failed";
                $comments = $e->getMessage();
                $failures++;
            }
            CampaignLog::create([
                                "customer_email_id" => $customer->id,
                                "template_id" => $this->listTemplate->template_id,
                                "list_template_id" => $this->listTemplate->id,
                                "from_address" => json_encode($this->from),
                                "status" => $status,
                                "comments" => $comments,
                                "message_id" => $messageId
                                ]);
        }
        $this->listTemplate->sent += $sent;

        $this->listTemplate->failures += $failures;

        $this->listTemplate->save();

        // finally update to complete
        $this->updateListTemplateStatus(self::STATUS_COMPLETE);
	}

    public function setMessage($params){

        $this->message = Swift_Message::newInstance($this->listTemplate->template->subject);

        $this->message->setFrom($this->from);

        $this->message->setTo($params["to"]);

        $this->message->setReplyTo($params["replayTo"]);

        $messageId = $this->message->getHeaders()->get("message-id")->getId();

        $params['message_id'] = $messageId;

        unset($params['to']);

        unset($params['replayTo']);

        if (view()->exists(self::TEMPLATE_PATH.".".$this->listTemplate->template_id)){
            //$bodyMessge = view(self::TEMPLATE_PATH.".".$this->listTemplate->template_id, $data = [])->render();
            $bodyMessge = base64_decode($this->listTemplate->template->body);
            $dom = new \DOMDocument;
            @$dom->loadHTML($bodyMessge);

            $links = $dom->getElementsByTagName('a');
            foreach ($links as $link) {
                $params['redirectUrl'] =  $link->getAttribute("href");
                $encodedParams = base64_encode(json_encode($params));
                if($link->getAttribute("data-unsubscribe")){
                    $link->setAttribute("href", \URL::to('/'). ":9000/api/unsubscribe?cec=".$encodedParams);
                }else {
                   $link->setAttribute("href", \URL::to('/'). ":9000/api/click?cec=".$encodedParams); 
                }
            }

            $html = $dom->saveHTML();
            $this->message->setBody($html, "text/html");
            $this->message->addPart($this->listTemplate->template->subject, 'text/plain');  
        }

        return $messageId;
    }

    public function setMailer(){
        $this->setSwiftTranporter();
        $this->swiftMailer = Swift_Mailer::newInstance($this->getSwiftTransporter());
    }

    public function setSwiftTranporter(){
         $this->swiftTransporter = Swift_SmtpTransport::newInstance()
          ->setHost(\Config::get("mail.host"))
          ->setPort(\Config::get("mail.port"))
          ->setEncryption(\Config::get("mail.encryption"))
          ->setUsername(\Config::get("mail.username"))
          ->setPassword(\Config::get("mail.password")); 

    }

    public function getSwiftTransporter(){
        return $this->swiftTransporter;
    }

    public function updateListTemplateStatus($status, $comments = null){
        $this->listTemplate->status = $status;
        $this->listTemplate->comments = $comments;
        $this->listTemplate->save();
    }

    public function  start(){
        $this->swiftMailer->getTransport()->start();   
    }

    public function  stop(){
        $this->swiftMailer->getTransport()->stop();   
    }
}
	

?>