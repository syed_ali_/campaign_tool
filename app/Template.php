<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
	protected $fillable = ['name', 'subject', 'body', 'user_id'];

	protected $table = "templates";


	public function user(){
		return $this->belongsTo('App\User');
	}

	public function lists(){
    	return $this->belongsToMany('App\EmailList', 'list_templates', 'list_id', 'template_id');
    }

    public function list_templates(){
    	return $this->hasMany("App\ListTemplate");
    }

   public function campaign_logs(){
        return $this->hasMany("App\CampaignLog");
    }

}