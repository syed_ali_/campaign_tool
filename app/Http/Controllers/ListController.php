<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CustomerEmail;

use App\EmailList;

use Exception;

use DB;

use Session;

class ListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){
    	$data['lists'] = \Auth::user()->email_lists->reverse();

    	foreach ($data['lists'] as $key => $list) {
    		$list->{"active"} = CustomerEmail::where(["list_id" => $list->id, "user_id" => \Auth::user()->id, "unsubscribed" => 0, "bounced" => 0])->count();
    		$list->{"bounced"} = CustomerEmail::where(["list_id" => $list->id, "user_id" => \Auth::user()->id, "bounced" => 1])->count();
    		$list->{"unsubscribed"} = CustomerEmail::where(["list_id" => $list->id, "user_id" => \Auth::user()->id, "unsubscribed" => 1])->count();
    	}

    	return view('lists.index')->with('data', $data);

    }

    public function create(){
        return view('lists._create_form');
    }

    public function store(Request $request){
    	$params = $request->input();

        DB::beginTransaction();

        try {
            $list = EmailList::create(["name" => $params["name"], "user_id" => \Auth::user()->id]);

            $tmpName = $_FILES['customer_emails']['tmp_name'];
            $arr = explode('.', $_FILES['customer_emails']['name']);

            $ext = strtolower(array_pop($arr));

            $type = $_FILES['customer_emails']['type'];

            $name = $_FILES['customer_emails']['name'];

            $insertData = array();

            $row = 0;

            if($ext == "csv"){
                if(($handle = fopen($tmpName, "r")) !== false){
                    $row = 0;
                    while(($data = fgetcsv($handle, 1000, ",")) !== false){
                        if($row > 0){
                            $insertData[$data[0].":".$data[1]] = array("email" => $data[0], "name" => $data[1], "list_id" => $list->id, "user_id" => \Auth::user()->id, "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s'));
                        }
                        $row++;
                    }
                }
            }

            if(!empty($insertData)){
                CustomerEmail::insert(array_values($insertData));
            }     
            unset($insertData);       
        } catch (Exception $e) {
            DB::rollback();
            Session::flash('message', $e->getMessage()); 
        }

        return redirect("/lists");
    }

    public function edit(){
        
    }

    public function update(){
        
    }

}    

?>