<?php 
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\CampaignLog;

use App\CustomerEmail;

class StatsController extends Controller
{

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function click(Request $request){
        $params = $request->input("cec");

        if($params){
            $params = (array)json_decode(base64_decode($params));
        }

        $campaignLogObj = CampaignLog::where(["message_id" => $params["message_id"]])->first();

        if(!empty($campaignLogObj)){
            $campaignLogObj->click = 1;
            $campaignLogObj->save();
            if($params['redirectUrl']){
                $urlSegments = parse_url($params['redirectUrl']);
                $redirectUrl = isset($urlSegments['scheme']) ? $urlSegments['scheme'] : "http";
                $redirectUrl .= "://".$urlSegments['host'];

                if(isset($urlSegments['port'])){
                    $redirectUrl .= ":".$urlSegments['port'];
                }

                $redirectUrl .=  $urlSegments['path'];

                $utmParams = "utm_campaign=".$params['utm_campaign']."&utm_medium=".$params["utm_medium"]."&utm_source=".$params["utm_source"];

                if(isset($urlSegments['query'])){
                    $redirectUrl .= "?".$urlSegments['query']."&". $utmParams;
                }else {
                    $redirectUrl .= "?". $utmParams;
                }
                if(isset($urlSegments['fragment'])){
                    $redirectUrl .= "#".$urlSegments['fragment'];
                }
                return redirect($redirectUrl);
            }
        }else {
            echo "Sorry, our system doesn't know you";
            return view("api.unsubscribe");
        }

    }


    public function unsubscribe(Request $request){
        $params = $request->input("cec");

        if($params){
            $params = (array)json_decode(base64_decode($params));
        }

        $campaignLogObj = CampaignLog::where(["message_id" => $params["message_id"]])->first();

        if(!empty($campaignLogObj)){
            CustomerEmail::where(["id" => $campaignLogObj->customer_email_id])
                        ->update(["unsubscribed" => 1]);
            $campaignLogObj->unsubscribed = 1;
            $campaignLogObj->save();
            echo "Your Email has been unsubscribed from newsletter";
        }else {
            echo "Sorry, our system doesn't know you";   
        }
        
        return view("api.unsubscribe");
    }

}

?>