<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log;

class NotifyController extends Controller
{
    
    public function snsResponse(Request $request){
    	$params = $request->input();
    	Log::info(json_encode($params));
    }
}
