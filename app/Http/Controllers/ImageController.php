<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\S3;

use Config;

class ImageController extends Controller
{
    
    public function uploadTemplateImages(Request $request){
    	$s3 = new S3(Config::get("s3.S3_ACCESS_KEY"), Config::get("s3.S3_SECRET_KEY"), false, Config::get("s3.S3_END_POINT"));

    	$tmpName = $_FILES['uploadImage']['tmp_name'];

        $type = $_FILES['uploadImage']['type'];

        $name = $_FILES['uploadImage']['name'];

        $filePath = public_path()."/template_images/".$name;

        file_put_contents($filePath, file_get_contents($tmpName));

        $output = $s3->putObjectFile($filePath, Config::get("s3.S3_TEMPLATE_IMAGE_BUCKET"), $name, 'public-read');

        unlink($filePath);

        $s3ImageUrl =  "http://".Config::get("s3.S3_END_POINT")."/".Config::get("s3.S3_TEMPLATE_IMAGE_BUCKET")."/".$name;

        return $s3ImageUrl;

        $data["image_url"] = $s3ImageUrl;

        return view("images.image_upload_response")->with("data", $data);
    }

    public function getLatestUplodedImage(){
    	return view("images/select_images");
    }

}
