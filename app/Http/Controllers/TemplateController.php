<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Template;

use Illuminate\Support\Facades\Artisan;

use App\User;

use App\ListTemplate;

use App\CampaignLog;

class TemplateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['templates'] = \Auth::user()->templates->reverse();
        foreach ($data['templates'] as $key => $template) {
            $template->{"clicks"} = CampaignLog::where(["template_id" => $template->id, "click" => 1])->count();
            $template->{"total"} = CampaignLog::where(["template_id" => $template->id])->count();
            $template->{"delivered"} = CampaignLog::where(["template_id" => $template->id, "status" => "Delivery"])->count();
            $template->{"bounced"} = CampaignLog::where(["template_id" => $template->id, "status" => "Bounce"])->count();
            $template->{"failed"} = CampaignLog::where(["template_id" => $template->id, "status" => "Failed"])->count();
        }
        return view('templates.index')->with('data', $data);
    }

    public function create()
    {
        return view('templates.create');
    }

    public function store(Request $request){
        $params = $request->input();

        $emailBody = $params['body'];
        
        $params['body'] = base64_encode($params['body']);

        $params['user_id'] = \Auth::user()->id;

        $newTemplate = Template::create($params);

        $handle = fopen(base_path('resources/views/mail/templates/'.$newTemplate->id.'.blade.php'),"w");
        fwrite($handle, $emailBody);

        fclose($handle);

        return redirect("/templates/preview?id=".$newTemplate->id);
    }
    /*
     * @request GET
    **/
    public function edit($id){
        $template = Template::findOrFail($id);
        return view('templates.edit')->withTemplate($template);
    }

    /*
     * @request PUT/PATCH
    **/

    public function update($id, Request $request){
        $template = Template::findOrFail($id);
        $input = $request->all();

        $input['body'] = base64_encode($input['body']);
        $template->fill($input)->save();
        return redirect("/templates/preview?id=".$id);
    }

    public function preview(Request $request){
        $params = $request->input();
        $data['template'] = Template::where(['id' => $params['id'], 'user_id' => \Auth::user()->id ])->first();
        $data['lists'] = \Auth::user()->email_lists;
        return view("templates.preview")->with("data", $data);
    }

    public function sendTestMail(Request $request){
        $params = $request->input();

        $template = Template::where(['id' => $params['templateId'], 'user_id' => \Auth::user()->id ])->first();

        if(!empty($template)){
            Artisan::call("send:email", ["templateId" => $params['templateId'], 'testEmail' => $params['testEmail']]);
        }

        return redirect("/templates/preview?id=".$template->id);
    }

    public function sendListMail(Request $request){
        $params = $request->input();

        foreach ($params['listIds'] as $key => $listId){
            $ltObj = ListTemplate::where(['list_id' => $listId, 'template_id' => $params['templateId']])->first();
            if(empty($ltObj)){
                ListTemplate::create(['list_id' => $listId, 'template_id' => $params['templateId'], 'scheduled_at' => $params['scheduled_at']]);     
            }
        }
        return redirect("/templates/preview?id=".$params['templateId']);
    }

    public function history(Request $request){
        $params = $request->input();
        $template = Template::where(['id' => $params['id'], 'user_id' => \Auth::user()->id ])->first();
        $data['listTemplates'] = $template->list_templates;
        return view("templates.history")->with("data", $data);
    }

}
