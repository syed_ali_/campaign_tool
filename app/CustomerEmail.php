<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerEmail extends Model
{

	protected $table = "customer_emails";

	protected $fillable = ["email", "name", "list_id", "user_id", "active", "unsubscribed", "bounced"];

	public function email_list(){
    	return $this->belongsTo('App\EmailList');
    }

    public function campaign_logs(){
    	return $this->hasMany("App\CampaignLog");
    }
}
