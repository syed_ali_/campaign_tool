<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Helpers\MailClient;

class TestSwiftMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $bodyMessge = view("mail.templates.1", $data = [])->render();

        $dom = new \DOMDocument;
        @$dom->loadHTML($bodyMessge);

        $links = $dom->getElementsByTagName('a');
        foreach ($links as $link) {
            
            if($link->getAttribute("data-unsubscribe")){
                $link->setAttribute("href", \URL::to('/'). ":9000/api/unsubscribe?cec=");
            }else {
                $link->setAttribute("href", $link->getAttribute("href") . "?utm_source='Indusdiva_Tool'");
            }
        }

        $html = $dom->saveHTML();

        echo $html;

        //MailClient::send();



        /*$transport = Swift_SmtpTransport::newInstance()
          ->setHost('smtp.example.org')
          ->setPort(587)
          ->setEncryption('ssl')
          ->setUsername('your username')
          ->setPassword('your password');
        
        $mailer = Swift_Mailer::newInstance($transport);

        $message = Swift_Message::newInstance('Wonderful Subject')
          ->setFrom(array('john@doe.com' => 'John Doe'))
          ->setBody('Here is the message itself')
          ;

        // Send the message
        $failedRecipients = array();
        $numSent = 0;
        $to = array('receiver@domain.org', 'other@domain.org' => 'A name');

        foreach ($to as $address => $name)
        {
          if (is_int($address)) {
            $message->setTo($name);
          } else {
            $message->setTo(array($address => $name));
          }

          $numSent += $mailer->send($message, $failedRecipients);
        }

        printf("Sent %d messages\n", $numSent);*/
    }
}
