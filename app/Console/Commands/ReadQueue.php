<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Aws\Sqs\SqsClient;

use Aws\Credentials\Credentials;

use App\CampaignLog;

use Config;

use App\CustomerEmail;

class ReadQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sqs:messages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $credentials = new Credentials(Config::get("sqs.key"), Config::get("sqs.secret"));

        $sqs = new SqsClient([
            'version'     => Config::get("sqs.version"),
            'region'      => Config::get("sqs.region"),
            'credentials' => $credentials
        ]);

        for($i = 0 ; $i < 500; $i++){

            $result = $sqs->receiveMessage(array("QueueUrl"=> Config::get("sqs.queue_url")));

            if($result->get("Messages")){

                $receiptHandle = $result->get("Messages")[0]['ReceiptHandle'];

                $message = (array) json_decode($result->get("Messages")[0]['Body']);

                $body = (array) json_decode($message['Message']);

                $notificationType = $body['notificationType'];

                $mail = (array) $body['mail'];

                $commonHeaders = (array) $mail['commonHeaders'];

                $swiftMessageId = substr(trim($commonHeaders['messageId']), 1, -1);

                $campaignLogObj = CampaignLog::where('message_id', $swiftMessageId)->first();

		$campaignLogObj->status = $notificationType;

		$campaignLogObj->save();


                if($notificationType == "Bounce"){
                    CustomerEmail::where("id", $campaignLogObj->customer_email_id)
                            ->update(["bounced" => 1]);
                }

                $result = $sqs->deleteMessage(array(
                    'QueueUrl' => Config::get("sqs.queue_url"),
                    'ReceiptHandle' => $receiptHandle,
                ));
            }
        }

       
    }
}
