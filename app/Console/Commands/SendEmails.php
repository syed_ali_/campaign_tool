<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\DB;

use App\Template;

use App\Helpers\MailClient;

use App\Helpers\EmailProcessor;

use Illuminate\Mail\Mailer as Mailer;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email  {templateId?} {testEmail?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending Emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $args = $this->argument();

        $templateId = $args['templateId'];

        $testEmail = isset($args['testEmail']) ? $args['testEmail'] : null;

        $listId = isset($args['listId']) ? $args['listId'] : null;

        $template = Template::find($templateId);

        if(!is_null($testEmail)){
            $email = $testEmail;
            $name = explode("@", $testEmail)[0];

            $subject = $template->subject;

            $data = [];

            $response = Mail::send("mail.templates.".$templateId, $data,function($message) use ($email, $name, $subject) {
                $message->to($email, $name)->subject($subject);
            });

        }else {
            EmailProcessor::start();
        }
       
    }
}
