<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListTemplate extends Model
{
   protected $table = "list_templates";

   protected $fillable = ['list_id', 'template_id', 'scheduled_at', 'comments'];

   public function email_list(){
    	return $this->belongsTo("App\EmailList", "list_id");
   }

   	public function template(){
    	return $this->belongsTo("App\Template", "template_id");
   	}

    public function campaign_logs(){
    	return $this->hasMany("App\CampaignLog");
    }

}
