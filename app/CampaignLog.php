<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignLog extends Model
{
	protected $fillable = ['customer_email_id', 'list_template_id', 'from_address', 'status', 'comments', 'message_id', 'template_id', "click", "unsubscribed"];

    protected $table = "campaign_logs";

    public function customer_email(){
    	return $this->belongsTo("App\CustomerEmail");
    }

    public function list_template(){
    	return $this->belongsTo("App\ListTemplate");
    }
}
