<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailList extends Model
{
	protected $table = "lists";

    protected $fillable = ['name', 'user_id'];


	public function customer_emails(){
        return $this->hasMany('App\CustomerEmail', 'list_id');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function templates(){
    	return $this->belongsToMany('App\Template', 'list_templates', 'list_id', 'template_id');
    }

    public function list_templates(){
        return $this->hasMany("App\ListTemplate");
    }
}
