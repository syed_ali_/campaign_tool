
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    	<div class="col-sm-6">
    		<div class="col-sm-12">
	    		<label for="emailSubject">Email Subject : </label>
		       	{{ $data['template']->subject }}
	       	</div>
	       	<div class="col-sm-12">
		       	<label for="emailBody">Email Body : </label>
				<?php if(View::exists('mail.templates.'.$data['template']->id)) : ?>
					<?php echo base64_decode($data['template']->body) ?>
				<?php else : ?>
					<p>No Email Body Exists</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="col-sm-6">
	       <form method="POST" id="sendTestMailForm" action="/templates/sendTestMail">
		     {{ csrf_field() }}
		    <div class="form-group">
		        <label for="testemail">Enter Test Email</label>
		        <input type="email" class="form-control" id="testEmail" name="testEmail" placeholder="Enter Test Email" value="" required>
		         <input type="hidden" class="form-control" id="templateId" name="templateId" value="{{$data['template']->id}}">
		    </div>
		    <button type="submit" class="btn btn-primary">Send Sample Email</button>
			</form>
			<hr>
			<form method="POST" id="sendListMailForm" action="/templates/sendListMail">
			     {{ csrf_field() }}

			    <div class="form-group">
				    <label for="list">Select List</label>
				    <select class="chosen form-control" multiple id="list_select" name="listIds[]" >
				      <?php foreach($data['lists'] as $list) : ?>
					    	<option value="<?php echo $list->id ?>"><?php echo $list->name ?></option>
					  <?php endforeach; ?>
					</select>
					<input type="hidden" class="form-control" id="templateId" name="templateId" value="{{$data['template']->id}}">
				 </div>
				 <div class="form-group">
				 	<label for="scheduleTime">Select Schedule Time (GMT)</label>
				 	<div class="row">
		        		<div class='col-sm-6'>
						 	<div class='input-group date' id='datetimepicker'>
				                <input type='text' class="form-control" id="scheduledAt" name="scheduled_at" required />
				                <span class="input-group-addon">
				                    <span class="glyphicon glyphicon-calendar"></span>
				                </span>
				            </div>
				        </div>
				    </div>
				 </div>
			    <button type="submit" class="btn btn-primary">Schedule Template</button>
			</form>
		</div>
    </div>
</div>
@endsection
