@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('templates._create_form') 
    </div>
</div>
@endsection
