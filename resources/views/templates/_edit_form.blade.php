<div class="row">
    <div class="col-sm-8">
		{!! Form::model($template, [
		    'method' => 'PATCH',
		    'route' => ['templates.update', $template->id]
		]) !!}

		<div class="form-group">
		    {!! Form::label('templateName', 'Template Name', ['class' => 'control-label']) !!}
		    {!! Form::text('name', $template->name, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('emailSubject', 'Email Subject:', ['class' => 'control-label']) !!}
		    {!! Form::text('name', $template->subject, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('exampleBody', 'Email Body Html:', ['class' => 'control-label']) !!}
		    <div class="panel with-nav-tabs panel">
		        <div class="panel-heading">
		            <ul class="nav nav-tabs">
		                <li class="active"><a href="#tab1primary" data-toggle="tab" id="html-tab">HTML</a></li>
		                <li class=""><a href="#tab2primary" data-toggle="tab" id="preview-tab">Preview</a></li>
		            </ul>
		        </div>
		        <div class="panel-body">
		            <div class="tab-content">
		                <div class="tab-pane fade active in" id="tab1primary">
		                    <textarea class="form-control" id="body" name="body" rows="10" required="">
		                    	<?php echo base64_decode($template->body) ?>
		                    </textarea>
		                </div>
		                <div class="tab-pane" id="tab2primary">
		                    <div class="row">
		                        <div id="email-body"></div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>

		{!! Form::submit('Update & Preview', ['class' => 'btn btn-primary']) !!}

		{!! Form::close() !!}
	</div>
	<div class="col-sm-4">
        <form id="updateImage" enctype='multipart/form-data'>
            <div class="form-group">
                <label for="image">Image</label>
                <img style="height:230px;width:230px;" id="display-image" src="" />
            </div>
            <div class="form-group">
                <label for="imageUrl">Image Url</label>
                <input type="text" class="form-control" name="imageUrl" value="">
            </div>
            <div class="form-group">
                <label for="chnageImage">Change Image</label>
                <input type="file" class="form-control" name="uploadImage">
            </div>
            <div class="form-group">
                <label for="redirectUrl">Redirect Url</label>
                <input type="text" class="form-control" name="redirectUrl" value="">
            </div>
            <button class="btn btn-primary">Update Image</button>
        </form>
    </div>
</div>

