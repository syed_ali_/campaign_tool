
@extends('layouts.app')

@section('content')
	<div class="container">
		<table class="table">
			<thead>
				<tr>
					<td>Template Name</td>
					<td>Email Subject</td>
					<td>Created At</td>
					<td>Actions</td>
					<td>Stats</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data['templates'] as $template) { ?>
					<tr>
						<td>{{$template->name}}</td>
						<td>{{$template->subject}}</td>
						<td>{{$template->created_at}}</td>
						<td>
						<span><a href="/templates/preview/?id={{$template->id}}">Preview</a></span> |
						<span>
							<a href="/templates/{{$template->id}}/edit">Edit</a> |
						</span>
						<span>
							<a href="/templates/history/?id={{$template->id}}" target="_blank" >History</a>
						</span>
						</td>
						<td>
							Clicks: {{$template->clicks}}
							Delivered: {{$template->delivered}}
							Bounce : {{$template->bounced}}
							Failed : {{$template->failed}}
							Total : {{$template->total}}
						</td>
					</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
	
@endsection