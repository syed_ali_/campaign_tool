<div class="row">
    <div class="col-sm-8">
        <form method="POST" id="createTemplate" action="/templates">
             {{ csrf_field() }}
            <div class="form-group">
                <label for="templateName">Template Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Template Name" value="" required>
            </div>
            <div class="form-group">
                <label for="emailSubject">Email Subject</label>
                <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Email Subject" required >
            </div>
            <div class="form-group">
                <label for="exampleBody">Email Body Html</label>
                <div class="panel with-nav-tabs panel">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1primary" data-toggle="tab" id="html-tab">HTML</a></li>
                            <li class=""><a href="#tab2primary" data-toggle="tab" id="preview-tab">Preview</a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab1primary">
                                <textarea class="form-control" id="body" name="body" rows="10" required=""></textarea>
                            </div>
                            <div class="tab-pane" id="tab2primary">
                                <div class="row">
                                    <div id="email-body"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit & Preview</button>
        </form>
    </div>
    <div class="col-sm-4">
        <form id="updateImage" enctype='multipart/form-data'>
            <div class="form-group">
                <label for="image">Image</label>
                <img style="height:230px;width:230px;" id="display-image" src="" />
            </div>
            <div class="form-group">
                <label for="imageUrl">Image Url</label>
                <input type="text" class="form-control" name="imageUrl" value="">
            </div>
            <div class="form-group">
                <label for="chnageImage">Change Image</label>
                <input type="file" class="form-control" name="uploadImage">
            </div>
            <div class="form-group">
                <label for="redirectUrl">Redirect Url</label>
                <input type="text" class="form-control" name="redirectUrl" value="">
            </div>
            <button class="btn btn-primary">Update Image</button>
        </form>
    </div>
</div>

