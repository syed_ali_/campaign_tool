@extends('layouts.app')

@section('content')
	<div class="container">
		<table class="table">
			<thead>
				<tr>
					<td>Template Name</td>
					<td>Email Subject</td>
					<td>List Name</td>
					<td>Scheduled At</td>
					<td>Created At</td>
					<td>Status</td>
					<td>Comments</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data['listTemplates'] as $listTemplate) { ?>
					<tr>
						<td>{{$listTemplate->template->name}}</td>
						<td>{{$listTemplate->template->subject}}</td>
						<td>{{$listTemplate->email_list->name}}</td>
						<td>{{$listTemplate->scheduled_at}}</td>
						<td>{{$listTemplate->created_at}}</td>
						<td>{{$listTemplate->status}}</td>
						<td>{{$listTemplate->comments}}</td>
					</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
	
@endsection