<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Campaign Project</title>

    <!-- Styles -->
    <!-- <link href="/css/app.css" rel="stylesheet"> -->

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />

    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link href="/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/chosen.css" type="text/css">

    <!-- <link rel="stylesheet" href="/css/neo.css" type="text/css"> -->

    <link href="/css/sdk.css" rel="stylesheet">
    
    <link href="/css/common.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="navbar">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="{{ url('/templates') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Templates <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <li><a href="{{ url('/templates') }}">All Templates</a></li>
                              <li><a href="{{ url('/templates/create') }}">Create Template</a></li>
                            </ul>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
   <!--  <script src="/js/app.js"></script>
 -->
    <script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script src="/js/moment-with-locales.js"></script>
    <script src="/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="/js/chosen.jquery.js"></script>

    <!-- <script type="text/javascript" src="/js/ckeditor.js"></script>

    <script src="http://sdk.ckeditor.com/ckfinder/ckfinder.js"></script>

     <script src="/js/picoModal-2.0.1.min.js"></script>
    <script src="/js/imageupload.js"></script>

    <script src="/js/contentloaded.js"></script>
     <script src="/js/simplesample.js"></script>
    <script src="/js/beautify-html.js"></script> -->

    <!-- 
        Not required for now can handle by html
    <script type="text/javascript" src="/js/jquery.validate.min.js"></script> -->

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker').datetimepicker({
                format : 'YYYY-MM-DD HH:mm:ss'
            });

            /*$("form#createTemplate").validate({
                rules: {
                    name : "required",
                    subject : "required",
                    body : "required"
                }
            });*/

           /* $("form#sendTestMailForm").validate({
                rules : {
                    testEmail: {
                        required: true,
                        email: true
                    }
                }
            });*/


            $(".chosen").data("placeholder","Select Lists ..").chosen().change(function(){
                $("div#list_select_chzn label.error").html("");
            });

            $("form#sendListMailForm").submit(function(){
                var returnType = true;
                if($("select[name='listIds[]']").val() == null ){
                    $("div#list_select_chzn").append('<label class="error">This field is required.</label>')
                    returnType = false;
                }else {
                    $("div#list_select_chzn label.error").html("");
                }
                return returnType;
            });
        });

        var templateHtml = "";

        $("li a#preview-tab").click(function(){
            templateHtml = $("div#tab1primary textarea#body").val();
            $("div#tab2primary").find("div#email-body").html($("div#tab1primary textarea#body").val());
            var allimages = $("div#tab2primary img");

            allimages.each(function(k,im){ 
                imageHost = im.src.split("/")[2];
                if(imageHost != "s3-ap-southeast-1.amazonaws.com"){
                    $(im).attr("src", "/image-not-found.png");
                }
            });

            var latestClick ;
            $("div#tab2primary img").click(function(e){
                e.preventDefault();
                $("form#updateImage img#display-image").attr("src", $(this).attr("src"));
                $("form#updateImage input[name='imageUrl']").val($(this).attr("src"));
                $("form#updateImage input[name='redirectUrl']").val($(this).closest("a").attr("href"));
                $("div#tab2primary img").css("border", "none");
                $(this).css({"border-style":"dashed","border-spacing":"5px"});
                latestClick = this;
            });
            $("form#updateImage").submit(function(event){
                event.preventDefault();
                $.ajax({
                    url: "/images/uploadTemplateImages",
                    type: "POST",
                    data:  new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                        $("form#updateImage img#display-image").attr("src", data);
                        $("form#updateImage input[name='imageUrl']").val(data);
                        $(latestClick).attr("src", data);
                        $("div#tab2primary img").css("border", "none");
                        templateHtml = $("div#tab2primary").find("div#email-body").html();
                        $("li a#html-tab").click(function(){
                            $("div#tab1primary textarea#body").val(templateHtml);
                        });
                    }

                });
            });

        });

        $("li a#html-tab").click(function(){
            $("div#tab1primary textarea#body").val(templateHtml);
            $("div#tab2primary img").css("border", "none");
        });

        
    </script>


</body>
</html>
