
@extends('layouts.app')

@section('content')
  <div class="container">   
    <div class="container">
        <form method="POST" id="createList" action="/lists" enctype='multipart/form-data'>
             {{ csrf_field() }}
            <div class="form-group">
              <label for="name">List Name</label>
              <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group">
              <label for="file">Upload Files</label>
              <input type="file" class="form-control" id="customer_emails" name="customer_emails">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
  </div>
@endsection
