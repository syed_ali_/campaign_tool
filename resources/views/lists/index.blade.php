
@extends('layouts.app')

@section('content')
	{{Session::get("message")}}
	<div class="container">
		<table class="table">
			<thead>
				<tr>
					<td>List Name</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data['lists'] as $list) { ?>
					<tr>
						<td>{{$list}}</td>
					</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
	
@endsection