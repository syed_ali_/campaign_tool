@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <form>
            <div class="form-group">
                <label for="templateName">Template Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Template Name" value="">
            </div>
            <div class="form-group">
                <label for="emailSubject">Email Subject</label>
                <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Email Subject">
            </div>
            <div class="form-group">
                <label for="exampleBody">Email Body Html</label>
                <textarea class="form-control" id="body" name="body" rows="5"></textarea>
            </div>
            <!-- <div class="form-check">
                <label class="form-check-label">
                  <input type="checkbox" class="form-check-input">
                  Send To All
              </label>
            </div> -->
            <button type="submit" class="btn btn-primary">Submit & Preview</button>
        </form>
    </div>
</div>
@endsection
