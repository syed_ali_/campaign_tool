<?php

return [

    'S3_ACCESS_KEY' => env("S3_ACCESS_KEY"),

    'S3_SECRET_KEY' => env("S3_SECRET_KEY"),

    'S3_END_POINT' => env("S3_END_POINT"),

    'S3_TEMPLATE_IMAGE_BUCKET' => env("S3_TEMPLATE_IMAGE_BUCKET")
];
