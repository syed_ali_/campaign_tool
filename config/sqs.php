<?php

return [

    'key' => env("SQS_KEY"),

    'secret' => env("SQS_SECRET"),

    'queue_url' => env("SQS_QUEUE_URL"),

    'version' => env("SQS_VERSION", "latest"),

    'region' => env("SQS_REGION", "us-east-1")
];
