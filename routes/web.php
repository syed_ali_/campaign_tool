<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get("/", "TemplateController@index");

Route::get("/home", "TemplateController@index");

Auth::routes();

Route::post('templates/sendTestMail', "TemplateController@sendTestMail");

Route::post('templates/sendListMail', "TemplateController@sendListMail");

Route::get("templates/preview", "TemplateController@preview");

Route::get("templates/history", "TemplateController@history");

Route::resource('templates', 'TemplateController');

Route::post("notify/snsResponse", "NotifyController@snsResponse");

Route::get("api/click", "Api\StatsController@click");

Route::get("api/unsubscribe", "Api\StatsController@unsubscribe");

Route::resource('lists', 'ListController');

Route::post("images/uploadTemplateImages", "ImageController@uploadTemplateImages");

Route::get("images/getLatestUplodedImage", "ImageController@getLatestUplodedImage");